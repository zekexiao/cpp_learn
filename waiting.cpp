#include <iostream>
#include <ctime>

int main(int argc, char *argv[])
{
    using namespace std;
    int delay;
    cout << "Input a delay : " << endl;
    cin >> delay;

    clock_t delay_sys = delay * CLOCKS_PER_SEC;

    cout << "Starting.." << endl;
    clock_t start = clock();
    while(clock() - start < delay_sys) 
        ;
    
    cout << "End..." << endl;

    return 0;
}
