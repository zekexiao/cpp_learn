#include <iostream>
#include <cstring>
#include <ostream>

int main(int argc, char *argv[])
{
    char origin_str[99];
    std::cin >> origin_str;
    char re_str[99];
    int j = 0;
    for(int i = strlen(origin_str) - 1; i >= 0; --i, ++j){
        re_str[j] = origin_str[i];
    }
    re_str[j] = '\0';

    std::cout << "origin = " << origin_str << std::endl;
    
    std::cout << "re = " << re_str;
    return 0;
}
